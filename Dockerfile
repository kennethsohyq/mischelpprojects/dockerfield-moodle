# PRODUCTION
FROM registry.gitlab.com/kennethsohyq/mischelpprojects/moodle-dockerfield-build:master

ADD moodle /opt/moodle

WORKDIR /opt/moodle
RUN mv config-prod.php config.php
RUN mkdir -p /data/moodle/dir && chown -R www-data:www-data /data/moodle/dir
RUN service apache2 start

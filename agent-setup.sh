#/bin/bash

# Configure SSH Keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDbFyf4N4X/Qd6pf+H8/K9OWWxLXWLZLubzZcIs/JukDzSft2qvqY5hsexF/8XNJjt8N1kaU/fni/O0dpsCfRaDO6dJ5FSn2BBgsHVKdMBawKujrOfLxSDlNd2LtA+hDs2xnkQwTElK6S0I0KBn/6MsAePI6C8U0CLGyxcP3uS3gAqHhWXFLqacWgDfo72QSsJ2UOqC/IZJK1ZWEAFADDZZbbNhXtnFYzQ/5RDwRpVk1DVZHDYX7zFWsxb+bZnOpumfxdxLx0gDHx7MKf+Ay3g+DTkEtlfcqGYraI/6/FYcsHKzk580PLhfvadQ9a9l9fRBAIDVOoEBXU2CogHoC6dlJBCSXqkVOYaI7kUrJACSAFlWi2vu3fKh4BN2yKZG14dr0oqTmk8PfHE+FOlzbIP5IiAeyOY8YgVVTs3E4WuUuZgJjvdIflAooG4Ub+qgp/1M+dHsboMdEH7CbtAG+1qTOvWwze1EQqyz5GlTLtT16ogI+s4qJ2x5jrsRMxzQDZ8= x" >> ~/.ssh/authorised_keys

# Set Timezone
sudo timedatectl set-timezone Asia/Singapore

# Setup Ramdisk
sudo mkdir /mnt/data
sudo mkdir /mnt/data_backup
sudo cp -v /etc/fstab /etc/fstab.backup
sudo echo 'tmpfs  /mnt/data  tmpfs  rw,size=4G  0   0' >> /etc/fstab


# Setup Ramdisk Sync
(cat <<- _EOF_
[Unit]
Before=umount.target

[Service]
Type=oneshot
User=root
ExecStartPre=/bin/chown -Rf teamcity /mnt/data/
ExecStart=/usr/bin/rsync -ar /mnt/data_backup/ /mnt/data/
ExecStop=/usr/bin/rsync -ar /mnt/data/ /mnt/data_backup/
ExecStopPost=/bin/chown -Rf teamcity /mnt/data_backup
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target

_EOF_
) > /lib/systemd/system/ramdisk-sync.service

sudo systemctl enable ramdisk-sync.service

# Mount Ramdisk
sudo mount -a

# Configure Cron to Mout Ramdisk on every reboot
crontab -l > mycron
echo "@reboot mount -a" >> mycron
crontab mycron
rm mycron

# Install Docker CE
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common zip unzip
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo docker run hello-world

# Install Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.25.5/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

# Install OpenJDK 11
sudo apt-get install -y openjdk-11-jdk

# Download TeamCity Agent
cd /opt
wget https://jira.itachi1706.com:8686/update/buildAgentFull.zip
sudo mkdir buildAgent
sudo cp buildAgentFull.zip ./buildAgent
cd buildAgent
sudo unzip buildAgentFull.zip
sudo rm buildAgentFull.zip
cd conf
sudo cp buildAgent.dist.properties buildAgent.properties
sudo sed -i 's|http://localhost:8111/|https://jira.itachi1706.com:8686/|g' buildAgent.properties
sudo sed -i 's|name=|name=TeamCityAgent|g' buildAgent.properties
sudo sed -i 's|../work|/mnt/data/work|g' buildAgent.properties
sudo sed -i 's|../temp|/mnt/data/temp|g' buildAgent.properties
cd /opt
sudo mv buildAgent /mnt
cd /mnt/buildAgent/bin
sudo chmod +x agent.sh
sudo ./agent.sh start

tail -f /mnt/buildAgent/logs/output.log 
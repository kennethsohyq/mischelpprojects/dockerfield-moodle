<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'database';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'moodle';
$CFG->dbpass    = 'moodleP@ssw0rd';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 13306,
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_unicode_ci',
);

$CFG->wwwroot   = 'http://localhost/moodle';
$CFG->dataroot  = '/data/moodle/dir';
$CFG->admin     = 'admin';

$CFG->phpunit_dbtype    = 'mysqli';      // 'pgsql', 'mariadb', 'mysqli', 'mssql', 'sqlsrv' or 'oci'
$CFG->phpunit_dblibrary = 'native';     // 'native' only at the moment
$CFG->phpunit_dbhost    = 'mysql';  // eg 'localhost' or 'db.isp.com' or IP
$CFG->phpunit_dbname    = 'moodle_test';     // database name, eg moodle
$CFG->phpunit_dbuser    = 'moodle';
$CFG->phpunit_dbpass    = 'moodleP@ssw0rd';
//$CFG->phpunit_dbuser    = 'root';   // your database username
//$CFG->phpunit_dbpass    = 'P@ssw0rd';   // your database password
$CFG->phpunit_prefix = 'phpu_';
$CFG->phpunit_dataroot = '/data/moodle/punit';
$CFG->phpunit_dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_unicode_ci',
);
$CFG->lang="en";

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!

<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle_test';
$CFG->dbuser    = 'mood';
$CFG->dbpass    = 'P@ssw0rd';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 5432,
  'dbsocket' => '',
  'dbcollation' => '',
);

$CFG->wwwroot   = 'http://localhost/moodle';
$CFG->dataroot  = '/data/moodle/dir';
$CFG->admin     = 'admin';

$CFG->phpunit_dbtype    = 'pgsql';      // 'pgsql', 'mariadb', 'mysqli', 'mssql', 'sqlsrv' or 'oci'
$CFG->phpunit_dblibrary = 'native';     // 'native' only at the moment
$CFG->phpunit_dbhost    = 'localhost';  // eg 'localhost' or 'db.isp.com' or IP
$CFG->phpunit_dbname    = 'moodle_test';     // database name, eg moodle
$CFG->phpunit_dbuser    = 'mood';   // your database username
$CFG->phpunit_dbpass    = 'P@ssw0rd';   // your database password
$CFG->phpunit_prefix = 'phpu_';
$CFG->phpunit_dataroot = '/data/moodle/punit';
$CFG->lang="en";

$CFG->behat_dbtype    = 'pgsql';
$CFG->behat_dblibrary = 'native';
$CFG->behat_dbhost    = 'localhost';                      // Database IP
$CFG->behat_dbname    = 'moodle_test';              // Database Schema for PHPUnit (MAKE SURE THIS EXISTS AS AN EMPTY DB)
$CFG->behat_dbuser    = 'mood';
$CFG->behat_dbpass    = 'P@ssw0rd';
$CFG->behat_dataroot = '/data/moodle/behat';
$CFG->behat_prefix = 'bht_';
$CFG->behat_wwwroot = 'http://127.0.0.1/moodle';

$CFG->behat_config = array(
  'Mac-Firefox' => array(
	  'suites' => array (
		  'default' => array(
			  'filters' => array(
				 'tags' => '~@_file_upload'
			  ),
		  ),
	  ),
	  'extensions' => array(
		  'Behat\MinkExtension' => array(
			  'selenium2' => array(
				  'browser' => 'firefox',
				  'capabilities' => array(
					  'platform' => 'OS X 10.6',
					  'version' => 20
				  )
			  )
		  )
	  )
  ),
  'Mac-Safari' => array(
	  'extensions' => array(
		  'Behat\MinkExtension' => array(
			  'selenium2' => array(
				  'browser' => 'safari',
				  'capabilities' => array(
					  'platform' => 'OS X 10.8',
					  'version' => 6
				  )
			  )
		  )
	  )
  )
);
$CFG->behat_profiles = [
        'default' => [
            'browser' => 'chrome',
            'wd_host' => 'http://selenium:4444/wd/hub',
            'extensions' => [
                'Behat\MinkExtension' => [
                    'selenium2' => [
                        'browser' => 'chrome',
                    ]
                ]
            ]
        ]
    ];

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
